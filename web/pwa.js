
// Register the ServiceWorker limiting its action to those URLs starting
// by `controlled`. The scope is not a path but a prefix. First, it is
// converted into an absolute URL, then used to determine if a page is
// controlled by testing it is a prefix of the request URL.
navigator.serviceWorker.register('service-worker.js');

// Signal that the worker is active.
navigator.serviceWorker.ready.then(console.log('Service worker ready!'));

const updatesChannel = new BroadcastChannel('fallience-keyring-updates');
updatesChannel.addEventListener('message', async (event) => {
  console.log(event);
  // TODO:
  // figure out when the stopwatch is running
  console.log('Forcing update')
  window.location.reload();
});
