import 'clipboard.dart';

import 'dart:html';
import 'dart:math';

// .secure() uses a cryptographically secure source of secure numbers - in this case, the window's Crypto object - see https://developer.mozilla.org/en-US/docs/Web/API/Crypto/getRandomValues
Random randomFactory = new Random.secure(); 

List passwordLengths = [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];

void init() {
  newPassword();
  document.getElementById('generatorOutput').addEventListener('click', handleClick);
}

void handleClick(Event e) {
  copyToClipboard(document.getElementById('generatorOutput').text);
  newPassword();
}

void newPassword() {
  // randomly pick a length from the list
  int passwordLength = passwordLengths[randomFactory.nextInt(passwordLengths.length)];

  document.getElementById('generatorOutput').text = secureStringDart(passwordLength, r"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#_=+$^");
}

String secureStringDart(int length, String availableChars) {
  // create an empty string so we can add to it later
  String secureString = "";

  // add a character from availableChars until the max length is hit
  for (var i = 0; i < length; i++) {
    secureString += availableChars[randomFactory.nextInt(availableChars.length)];
  }
  return secureString;
}