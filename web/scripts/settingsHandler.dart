@JS()
library main;

import 'dart:html';
import 'dart:convert';
import 'package:js/js.dart';
import 'package:encrypt/encrypt.dart';

// wrap around download.js
@JS('download')
external void download(String fileContents, String fileName);

void initSettingsHandler() {
  if (window.localStorage['encrypted'] == 'false') {
    document.getElementById('settingsEncryptSetPasswordButtonString').classes.add('local_settingsEncryptSetPasswordButtonString');
    document.getElementById('settingsEncryptSetPasswordButtonString').onClick.listen(
      (event) => enableEncryption()
    );
  } else {
    document.getElementById('settingsEncryptSetPasswordButtonString').classes.add('local_settingsEncryptRemovePasswordButtonString');
    document.getElementById('settingsEncryptSetPasswordButtonString').onClick.listen(
      (event) => disableEncryption()
    );
  }

  document.getElementById('settingsExportButton').onClick.listen(
    (event) => exportData()
  );
  initImportStuff();

  document.getElementById('settingsResetButton').onClick.listen(
    (event) => resetApp()
  );
}

void disableEncryption() {
  final iv = window.localStorage['iv'];
  final encrypter = new Encrypter(new Salsa20(window.document.getElementById('main').dataset['password'], iv));

  window.localStorage['encrypted'] = "false";
  window.localStorage.remove('encryptionTest');
  window.localStorage['accountList'] = encrypter.decrypt(window.localStorage['accountList']);

  window.location.reload();
}

void enableEncryption() {
  var enableEncryptionPrompt = document.getElementById('enableEncryptionPasswordPrompt');
  InputElement enableEncryptionPasswordPromptTextField = document.getElementById('enableEncryptionPasswordPromptTextField');
  InputElement enableEncryptionPasswordPromptVerifyTextField = document.getElementById('enableEncryptionPasswordPromptVerifyTextField');
  enableEncryptionPrompt.classes.add("activeEnableEncryptionPrompt");

  void lockKeyring() {
    // check if it's valid
    if (enableEncryptionPasswordPromptTextField.value.length < 16) {
      window.document.getElementById('enableEncryptionPasswordPromptIncorrectPasswordString').classes.add('stringVisible');
    } else {
      try {
        window.document.getElementById('enableEncryptionPasswordPromptIncorrectPasswordString').classes.remove('stringVisible');
      } catch (e) {
        print('h');
      }
      if (enableEncryptionPasswordPromptTextField.value != enableEncryptionPasswordPromptVerifyTextField.value) {
        window.document.getElementById('enableEncryptionPasswordPromptPasswordsDontMatchString').classes.add('stringVisible');
      } else {
        // this is where the fun stuff happens!
        window.localStorage['encrypted'] = 'true';

        final iv = window.localStorage['iv'];
        final encrypter = new Encrypter(new Salsa20(enableEncryptionPasswordPromptTextField.value, iv));
        
        window.localStorage['encryptionTest'] = encrypter.encrypt('Correct Horse Battery Staple');
        window.localStorage['accountList'] = encrypter.encrypt(window.localStorage['accountList']);
        window.location.reload();
      }
    }
  }

  document.getElementById('enableEncryptionPasswordPromptGo').onClick.listen(
    (event) => lockKeyring()
  );
}

void exportData() {
  // build Map of data to be exported
  Map exportData = {
    "language": window.localStorage['language'],
    "iv": window.localStorage['iv'],
    "encryptionTest": window.localStorage['encryptionTest'],
    "encrypted": window.localStorage['encrypted'],
    "accountList": window.localStorage['accountList'],
  };

  // convert data to string
  final String exportString = jsonEncode(exportData);

  // get friendly time
  String now = new DateTime.now().toLocal().toString();
  String timeString = now.substring(0, 19); // get only date and time to the minute, skip milliseconds

  download(exportString, 'Keyring data from ' + timeString + '.keyring');
}

void initImportStuff() {
  // insane bodge that clicks the actual (and hidden) file input when the Import button in the settings is clicked
  // this is done so we can keep better UI
  document.getElementById('settingsImportButton').onClick.listen(
    (event) => document.getElementById('actualImportSelector').dispatchEvent(new MouseEvent('click'))
  );

  // this will be executed when the .keyring file is selected
  void actuallyImport(String keyringData) {
    Map keyringMap = jsonDecode(keyringData);
    keyringMap.keys.forEach((element) => window.localStorage[element] = keyringMap[element]); 
    window.location.reload();
  }

  // shamelessly taken from https://stackoverflow.com/a/13306671
  // thanks, Alex!
  InputElement inputFiles = document.getElementById('actualImportSelector');
  inputFiles.onChange.listen((e) {
    // read file content as text file
    final files = inputFiles.files;
    if (files.length == 1) {
      final file = files[0];
      final reader = new FileReader();
      reader.onLoad.listen((e) {
        actuallyImport(reader.result);
      });
      reader.readAsText(file);
    }
  });
}

void resetApp() {
  window.localStorage.clear();
  window.location.reload();
}