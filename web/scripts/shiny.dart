// wrapper for shiny.js
@JS()
library shiny;

import 'package:js/js.dart';

@JS('shinyNoConf')
external void shiny(String selector);