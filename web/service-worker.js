// just change this if you do something with the app
var appVersion = "1"

importScripts('workbox/workbox-sw.js');

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}

workbox.routing.registerRoute(
  new RegExp('/'),
  workbox.strategies.staleWhileRevalidate({
    cacheName: 'fallience-keyring-cache',
    plugins: [
      new workbox.broadcastUpdate.Plugin('fallience-keyring-updates')
    ]
  })
);

workbox.core.setLogLevel(4);
console.log(workbox.core.LOG_LEVELS)